var express = require('express');
const router = express.Router();


router.get('/', async function (req, res, next) {
    try {
        const User = Parse.Object.extend("User");
        const query = new Parse.Query(User);
        query.limit(100000)
        const results = await query.find();
        res.send({ status: 'success', message: "ok", data: results })
    } catch (error) {
        res.status(400).json({ status: 'error', message: error.message });
    }
});

router.post('/signup', async function (req, res, next) {
    const { username,name, status, phone,password } = req.body
    try {
        const user = new Parse.User();
        user.set('password',password)
        user.set("username", username);
        user.set("name", name);
        user.set("phone", phone);
        user.set("status", status);
        const saveObj = await user.signUp()
        res.send({ status: 'success', message: 'save done', object: saveObj });
    } catch (error) {
        res.status(400).json({ status: 'error', message: error.message });
    }

});


module.exports = router