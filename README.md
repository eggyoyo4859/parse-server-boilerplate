# Parse-Server-Boilerplate

Parse-Server template for all our back-end server

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.
#### With the Heroku Button

[![Deploy](https://www.herokucdn.com/deploy/button.png)](https://heroku.com/deploy)

### Prerequisites

What things you need to install the software and how to install them
- NodeJS
- Git
- PM2

### Installing

A step by step series of examples that tell you how to get a development env running
to get start fun, Use this CLI
```
git clone https://gitlab.com/eggyoyo4859/parse-server-boilerplate.git YOUR-NEW-PROJECT-NAME
cd YOUR-NEW-PROJECT-NAME
```
then use this command to prevent git change
```
git update-index --assume-unchanged .env
```
next step, we can use git command without authen in the next process
```
git config credential.helper store
```

config .env file for access Mongo-DB and set app key
```
nano .env
```

next, Install package
```
npm i --unsafe-perm=true
```

Now, We can start the project by
```
pm2 start pm2.json
```

## Deployment

Add additional notes about how to deploy this on a live system
